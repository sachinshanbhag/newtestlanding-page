import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Layout, Menu, Breadcrumb,Card,Button } from 'antd';

const { Header, Content, Footer } = Layout;
var HtmlToReactParser = require('html-to-react').Parser;

var baseurl = "https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1"
//var baseurl = "http://localhost:3002/api/v1/"
function ExplainerLandingPage(props) {
    const [ExplainerVideos, setExplainerVideos] = useState([])
    const [website, setWebsite] = useState('')
    const [CompanyName, setCompanyName] = useState('')
    const [logoimg, setlogoimg] = useState('')

    useEffect(() => {
        async function fetch() {
            try {
                var res = await axios.get(`${baseurl}/explainer/catalogRequestedVideosLandingPage/${props.match.params.id}`);
              //  var res = await axios.get(`${baseurl}/explainer/catalogRequestedVideosLandingPage/32e065c2-5274-420f-ac15-1f288d316e7e`);
                console.log(res.data.Items)
                setExplainerVideos(res.data.Items)
                setCompanyName(res.data.Items[0].CompanyName)
                console.log(res)
                try {
                    var res2 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/company/getlogo/${res.data.Items[0].CompanyId}`);
                    setlogoimg("data:image/*;base64," + res2.data.base64)
                }
                catch(e)
                {
                }
            } catch (error) {

            }
          
           
        }
        

        fetch()
    }, [])
    return (
        <Layout style={{ backgroundColor: 'white' }}>
            <Header calssName="center" style={{ position: 'fixed', zIndex: 1, width: '100%', boxShadow: "2px 5px 9px #888887", backgroundColor: "white"}}>
                     <div className="logo center" >{logoimg ? <img src={logoimg} style={{ height: "50px", width: "auto", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "6px" }} /> :
          <h4 style={{marginTop:"9px"}} calssName="center">{CompanyName}</h4>}</div>
            </Header>

            <div style={{display:"flex", justifyContent:'center',marginTop:"100px"}}>
                
   
                {ExplainerVideos ? ExplainerVideos.map(data => {
                     let script1 = document.createElement("script1");
                     
                     script1.src = `${data.Script1}`;
                   
                     script1.async = true;
                  
                     document.body.appendChild(script1);
                     let script = document.createElement("script");
                     
                     script.src = `${data.Script}`;
                   
                     script.async = true;
                  
                     document.body.appendChild(script);
                    
                    var htmlInput = data.customEC
                    var htmlToReactParser = new HtmlToReactParser();
                    var reactElement = htmlToReactParser.parse(htmlInput);
                    return (
                        <div className="ant-col ant-col-xs-20 ant-col-sm-20 ant-col-xl-12">
                            {/* <Card bordered={false} style={{ width: 323 }}> */}
                                <h4 style={{display:"flex", justifyContent:'center'}}>{data.Title}</h4>
                                <p>{data.Description && data.Description}</p>
                                {reactElement ?  reactElement : 
                                null
                                
                            }
                                
                        
                        </div>

                    )
                }):<div><img src={require("./220.gif") }/><p style={{paddingLeft: "62px"}}>Loading...</p></div>}
    
                 
            </div>
            <br></br>
           
                <Footer style={{ textAlign: 'center', fontSize: '18px' }}>
                    <p style={{ cursor: 'pointer', color: "blue" }} onClick={() => window.open(`https://${website}`, "_blank")}>{website}</p>
                    ©{(new Date().getFullYear())} {CompanyName}. All rights reserved</Footer>
        </Layout>
    )
}
export default ExplainerLandingPage